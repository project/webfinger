<?php

namespace Drupal\webfinger\Event;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\webfinger\JsonRd;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines a Webfinger event.
 */
class WebfingerResponseEvent extends Event {

  /**
   * The JSON resource descriptor.
   *
   * @var \Drupal\webfinger\JsonRd
   */
  protected $jsonRd;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The parsed request parameters.
   *
   * @var array
   */
  protected $params = [];

  /**
   * The response cacheability metadata.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $responseCacheability;

  /**
   * Constructs a new WebfingerEvent.
   *
   * @param \Drupal\webfinger\JsonRd $json_rd
   *   The JSON resource descriptor.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param string[] $params
   *   The parsed request parameters.
   * @param \Drupal\Core\Cache\CacheableMetadata $response_cacheability
   *   Collects cacheability for the query.
   */
  public function __construct(JsonRd $json_rd, Request $request, array $params, CacheableMetadata $response_cacheability) {
    $this->jsonRd = $json_rd;
    $this->request = $request;
    $this->params = $params;
    $this->responseCacheability = $response_cacheability;
  }

  /**
   * Returns the JSON resource descriptor.
   *
   * @return \Drupal\webfinger\JsonRd
   *   The JSON resource descriptor.
   */
  public function getJsonRd(): JsonRd {
    return $this->jsonRd;
  }

  /**
   * Returns the current request.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The current request.
   */
  public function getRequest(): Request {
    return $this->request;
  }

  /**
   * Returns the parsed request parameters.
   *
   * @return string[]
   *   The parsed request parameters.
   */
  public function getParams(): array {
    return $this->params;
  }

  /**
   * Returns the response cacheability.
   *
   * @return \Drupal\Core\Cache\CacheableMetadata
   *   Collected cacheability for the response.
   */
  public function getResponseCacheability(): CacheableMetadata {
    return $this->responseCacheability;
  }

}
