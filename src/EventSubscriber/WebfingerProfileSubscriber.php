<?php

namespace Drupal\webfinger\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\webfinger\Event\WebfingerResponseEvent;
use Drupal\webfinger\JsonRdLink;
use Drupal\webfinger\WebfingerEvents;
use Drupal\webfinger\WebfingerParameters;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Webfinger subscriber.
 */
class WebfingerProfileSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(AccountInterface $account, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    $this->account = $account;
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->logger = $logger;
  }

  /**
   * Builds a profile response.
   *
   * @param \Drupal\webfinger\Event\WebfingerResponseEvent $event
   *   The event to process.
   */
  public function onBuildResponseBuildProfile(WebfingerResponseEvent $event) {
    /** @var \Drupal\webfinger\JsonRd $json_rd */
    $json_rd = $event->getJsonRd();
    $request = $event->getRequest();
    $params = $event->getParams();
    $response_cacheability = $event->getResponseCacheability();

    // Subject should always be set.
    $subject = $request->query->get('resource') ?: '';
    if (!empty($subject)) {
      $json_rd->setSubject($subject);
    }

    // Determine if there is a user account path for a requested name.
    if (isset($params[WebfingerParameters::ACCOUNT_KEY_NAME]) && $user = $this->getUserByName($params[WebfingerParameters::ACCOUNT_KEY_NAME])) {
      // Determine if the current user has access to the requested user's
      // account.
      // Access differs by user so cache per user.
      $response_cacheability->addCacheContexts(['user']);
      if ($user->access('view', $this->account)) {
        // Prevent early rendering.
        $url = Url::fromRoute('entity.user.canonical', ['user' => $user->id()], ['absolute' => TRUE])->toString(TRUE);
        $response_cacheability->addCacheableDependency($url);
        $account_href = $url->getGeneratedUrl();
        $json_rd->addAlias($account_href);
        $link = new JsonRdLink();
        $link->setRel('http://webfinger.net/rel/profile-page')
          ->setType('text/html')
          ->setHref($account_href);
        $json_rd->addLink($link);
      }
      else {
        $this->logger->notice('Access denied for requested account "%name".', ['%name' => $params[WebfingerParameters::ACCOUNT_KEY_NAME]]);
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Run early so other implementations can add and alter.
    $events[WebfingerEvents::WEBFINGER_BUILD_RESPONSE][] = [
      'onBuildResponseBuildProfile',
      1000,
    ];
    return $events;
  }

  /**
   * Gets the user for a given account name.
   *
   * @param string $name
   *   The name of a requested account.
   *
   * @return \Drupal\user\UserInterface|null
   *   A fully-loaded user object upon successful user load or FALSE if user
   *   cannot be loaded
   */
  protected function getUserByName($name): ?UserInterface {
    $users = $this->userStorage
      ->loadByProperties(['name' => $name]);
    if ($users) {
      return reset($users);
    }

    return NULL;
  }

}
