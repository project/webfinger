<?php

namespace Drupal\webfinger;

/**
 * Defines events for the webfinger module.
 *
 * @see \Drupal\webfinger\Event\WebfingerEvent
 *
 * @internal
 */
final class WebfingerEvents {

  /**
   * Name of the event when generating a webfinger response.
   *
   * This event allows modules to add content to a webfinger response.
   *
   * @Event
   *
   * @see \Drupal\webfinger\Event\WebfingerResponseEvent
   * @see \Drupal\webfinger\WebfingerController::handleRequest()
   *
   * @var string
   */
  const WEBFINGER_BUILD_RESPONSE = 'webfinger.build_response';

}
