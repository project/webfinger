<?php

namespace Drupal\webfinger;

use Drupal\Core\Cache\CacheableMetadata;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Parses Webfinger parameters from a request.
 */
class WebfingerParameters {

  /**
   * The name of a host being requested.
   *
   * @var string
   */
  const HOST_KEY_NAME = 'host';

  /**
   * The name of an account being requested.
   *
   * @var string
   */
  const ACCOUNT_KEY_NAME = 'account';

  /**
   * The rel value.
   *
   * @var string
   */
  const REL_KEY_NAME = 'rel';

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The response cacheability metadata.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $responseCacheability;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Sets the request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return $this
   *   The called Webfinger parameters object.
   */
  public function setRequest(Request $request): self {
    $this->request = $request;
    return $this;
  }

  /**
   * Sets the response cacheability.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata $response_cacheability
   *   Collected cacheability for the response.
   */
  public function setResponseCacheability(CacheableMetadata $response_cacheability): self {
    $this->responseCacheability = $response_cacheability;
    return $this;
  }

  /**
   * Sets the logger.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   *
   * @return $this
   *   The called Webfinger parameters object.
   */
  public function setLogger(LoggerInterface $logger): self {
    $this->logger = $logger;
    return $this;
  }

  /**
   * Extracts Webfinger query parameters from the request.
   *
   * @return array
   *   An array of parameters.
   */
  public function getParams(): array {
    assert($this->request instanceof Request, 'Request must be set before calling ::getParams().');

    $params = [];
    $cache_contexts = [];
    if ($this->request->query->has('resource')) {
      $cache_contexts[] = 'url.query_args:resource';
      $resource = $this->request->query->get('resource');
      // @todo is HTTP_HOST the right value to check here?
      $host = $this->request->server->get('HTTP_HOST');
      // Convert URL if needed into a form for which 'user' and 'host' are
      // parsed.
      if (strpos($resource, '//') === FALSE) {
        $resource = str_replace(':', '://', $resource);
      }
      // Allow discovery without acct: scheme.
      if (strpos($resource, 'acct:') === FALSE) {
        $resource = 'acct://' . $resource;
      }
      $url = parse_url($resource);
      if (isset($url['scheme']) && isset($url['user']) && isset($url['host'])) {
        $params[static::HOST_KEY_NAME] = $host;
        switch ($url['scheme']) {
          case 'acct':
            // Ensure the request is for this domain.
            // @todo support comparison of subdomain?
            if ($host === $url['host']) {
              $params[static::ACCOUNT_KEY_NAME] = $url['user'];
            }
            else {
              $this->logger->notice('Requested host "%url_host" does not match actual host "%host".', [
                '%url_host' => $url['host'],
                '%host' => $host,
              ]);
            }
            break;
        }
      }
    }
    if ($this->request->query->has('rel')) {
      $cache_contexts[] = 'url.query_args:rel';
      // Standardize as an array.
      $params[static::REL_KEY_NAME] = (array) $this->request->query->get('rel');
    }
    $this->responseCacheability->addCacheContexts($cache_contexts);

    return $params;
  }

}
