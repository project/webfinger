<?php

namespace Drupal\webfinger\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\webfinger\Event\WebfingerResponseEvent;
use Drupal\webfinger\JsonRd;
use Drupal\webfinger\WebfingerEvents;
use Drupal\webfinger\WebfingerParameters;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Webfinger Controller.
 */
final class WebfingerController extends ControllerBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The webfinger logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a \Drupal\aggregator\Controller\AggregatorController object.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, LoggerInterface $logger) {
    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('event_dispatcher'),
      $container->get('logger.factory')->get('webfinger')
    );
  }

  /**
   * Handle request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Information about the current HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function handleRequest(Request $request) {
    $response_cacheability = new CacheableMetadata();
    $params = (new WebfingerParameters())
      ->setRequest($request)
      ->setResponseCacheability($response_cacheability)
      ->setLogger($this->logger)
      ->getParams();

    $json_rd = new JsonRd();
    $event = new WebfingerResponseEvent($json_rd, $request, $params, $response_cacheability);
    $this->eventDispatcher->dispatch($event, WebfingerEvents::WEBFINGER_BUILD_RESPONSE);

    if (!empty($event->getJsonRd()->getLinks())) {
      $response = new CacheableJsonResponse($event->getJsonRd()->toArray());
      $response->addCacheableDependency($response_cacheability);
    }
    else {
      // If no links were returned, set a not found status code.
      $response = new JsonResponse();
      $response->setStatusCode(404);
      $response->headers->set('Status', '404 Not Found');
    }

    $response->headers->set('Content-Type', 'application/jrd+json; charset=utf-8');
    $response->headers->set('Access-Control-Allow-Origin', '*');

    return $response;
  }

  /**
   * Host meta routing callback.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   A cacheable response.
   */
  public function hostMeta() {
    $response_cacheability = new CacheableMetadata();

    // We don't add the resource query param in here because it comes back
    // encoded.
    $url = Url::fromRoute('webfinger.well-known', [], ['absolute' => TRUE])->toString(TRUE);
    $response_cacheability->addCacheableDependency($url);

    $data = "<?xml version='1.0' encoding='UTF-8'?>
<XRD xmlns='http://docs.oasis-open.org/ns/xri/xrd-1.0'>
  <Link rel='lrdd' type='application/json'
        template='" . $url->getGeneratedUrl() . "?resource={uri}' />
</XRD>";

    $response = new CacheableResponse($data, 200, ['content-type' => 'application/xrd+xml']);
    $response->addCacheableDependency($response_cacheability);
    return $response;
  }

}
