<?php

declare(strict_types=1);

namespace Drupal\Tests\webfinger\Functional;

/**
 * Tests Webfinger functionality.
 *
 * @group webfinger
 */
final class WebfingerTest extends WebfingerTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['webfinger'];

  /**
   * Test webfinger response.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testWebfingerResponse(): void {
    $this->webfingerResponseHelper();
  }

}
