<?php

declare(strict_types=1);

namespace Drupal\Tests\webfinger\Functional;

/**
 * Tests Webfinger localized.
 *
 * @group webfinger
 */
final class WebfingerLocalizedTest extends WebfingerTestBase {

  /**
   * {@inheritdoc}
   */
  const LANGCODE = 'es';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language', 'webfinger'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupLanguage(static::LANGCODE);
  }

  /**
   * Test webfinger response.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testWebfingerResponse(): void {
    $this->webfingerResponseHelper();
  }

}
