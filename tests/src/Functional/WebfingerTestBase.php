<?php

declare(strict_types=1);

namespace Drupal\Tests\webfinger\Functional;

use Drupal\Core\Url;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Tests Webfinger bas functionality.
 *
 * @group webfinger
 */
class WebfingerTestBase extends BrowserTestBase {

  /**
   * The language to use for this test, if provided.
   *
   * @var string
   */
  const LANGCODE = '';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['webfinger'];

  /**
   * The administrator.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * The authenticated user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $authenticatedUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\user\RoleInterface $role */
    $role = Role::load(RoleInterface::ANONYMOUS_ID);
    $this->grantPermissions($role, ['access user profiles']);

    $this->adminUser = $this->createUser([], 'administrator', TRUE);
    $this->authenticatedUser = $this->createUser([], 'authenticated', FALSE);
  }

  /**
   * Get resource url.
   *
   * @param \Drupal\user\Entity\User $account
   *   An user account.
   * @param bool $addAcctUriScheme
   *   A boolean whether to add URI scheme.
   *
   * @return string
   *   A resource URL string.
   */
  protected function getResourceUrl($account, $addAcctUriScheme = TRUE) {
    $base_url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    return ($addAcctUriScheme ? 'acct:' : '') . $account . '@' . parse_url($base_url, PHP_URL_HOST);
  }

  /**
   * Setup language.
   *
   * @param string $langcode
   *   The langcode string.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setupLanguage($langcode) {
    ConfigurableLanguage::createFromLangcode($langcode)->save();
    \Drupal::configFactory()
      ->getEditable('language.types')
      ->set('negotiation.language_interface.enabled.language-session', -6)
      ->save();
    $this->rebuildContainer();
  }

  /**
   * Test Webfinger response.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function webfingerResponseHelper(): void {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    $query = static::LANGCODE ? ['language' => static::LANGCODE] : [];

    $this->drupalGet('/.well-known/webfinger');
    $assert_session->statusCodeEquals(404);
    $assert_session->responseHeaderContains('Content-Type', 'application/jrd+json; charset=utf-8');

    $resource = $this->getResourceUrl($this->adminUser->label());
    $this->drupalGet('/.well-known/webfinger', [
      'query' => ['resource' => $resource] + $query,
    ]);
    $assert_session->statusCodeEquals(200);
    $content = json_decode($page->getContent());
    self::assertEquals(Url::fromRoute('entity.user.canonical', [
      'user' => $this->adminUser->id(),
    ], [
      'absolute' => TRUE,
      'query' => $query,
    ])->toString(), $content->aliases[0]);

    $resource = $this->getResourceUrl($this->authenticatedUser->label(), FALSE);
    $this->drupalGet('/.well-known/webfinger', [
      'query' => ['resource' => $resource] + $query,
    ]);
    $assert_session->statusCodeEquals(200);
    $content = json_decode($page->getContent());
    self::assertEquals(Url::fromRoute('entity.user.canonical', [
      'user' => $this->authenticatedUser->id(),
    ], [
      'absolute' => TRUE,
      'query' => $query,
    ])->toString(), $content->aliases[0]);
  }

}
